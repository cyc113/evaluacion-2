package com.programadorinacap.certamen2;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.programadorinacap.certamen2.vistas.fragmentos.ContactoFragment;
import com.programadorinacap.certamen2.vistas.fragmentos.PersonalFragment;
import com.programadorinacap.certamen2.vistas.fragmentos.SesionFragment;

public class MainActivity extends AppCompatActivity implements ContactoFragment.OnFragmentInteractionListener,
        PersonalFragment.OnFragmentInteractionListener,SesionFragment.OnFragmentInteractionListener{

    PersonalFragment Frag1;
    ContactoFragment Frag2;
    SesionFragment Frag3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Frag1=new PersonalFragment();
        Frag2=new ContactoFragment();
        Frag3= new SesionFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.contenedor, Frag1).commit();

}

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
